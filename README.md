# DHBW KFZ-Blockchain Projekt

A Ethereum blockchain project for maintaining owner, mileage and transaction of cars

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to run the project

```
Node/npm >>> https://nodejs.org/
Truffle >>> https://www.trufflesuite.com/truffle
Ganache >>> https://www.trufflesuite.com/ganache
MetaMask >>> https://metamask.io/
Web3 >>> https://web3js.readthedocs.io/en/v1.2.4/
```

### Installation instructions

#### Linux/Windows

Download all dependencies in the app directory, e.g.

```sh
cd app
npm install
```

Start [Ganache](https://www.trufflesuite.com/ganache) on your device. 
Afterwards, run

```sh
cd ../truffle
truffle migrate
cd ../app
npm run-script dev
```

Now, the frontend should be running on `localhost:8080`.

To connect your Ganache wallets to project, have [Metamask](https://metamask.io/) started in your browser.

#### macOS

Download all dependencies in the app directory, e.g.

```sh
cd app
yarn
```

Start [Ganache](https://www.trufflesuite.com/ganache) on your device. 
Afterwards, run

```sh
cd ../truffle
yarn run truffle migrate
cd ../app
yarn start
```

Now, the frontend should be running on `localhost:8080`.

To connect your Ganache wallets to project, have [Metamask](https://metamask.io/) started in your browser.


## Running the tests

Use following command in the truffle directory to run the automated tests for this project

`Please make sure that Ganache is not started, otherwise the truffle tests won't be able to create a new test net`

```sh
truffle test
```


## Authors

* **Jan Rymkuß** - [jan.rymkuss](https://gitlab.com/jan.rymkuss)
* **Tobias Winkler** - [scholl1](https://gitlab.com/scholl1)
* **Stephan Killgus** - [Stevee205](https://gitlab.com/Stevee205)
* **Ali Dinc** - [dincali](https://gitlab.com/dincali)

See also the list of [contributors](https://gitlab.com/jan.rymkuss/kfz-blockchain/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License.

## Acknowledgments

* DHBW Blockchain lecture
* [DHBW Truffle Project](https://github.com/mkqavi/dhbw-truffle-project)
