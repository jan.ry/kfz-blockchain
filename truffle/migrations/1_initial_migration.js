const Migrations = artifacts.require("Migrations");
const CarClass = artifacts.require("CarClass")

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(CarClass);
};
