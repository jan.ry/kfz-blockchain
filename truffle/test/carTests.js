const CarContract = artifacts.require("CarClass");

contract("CarClass", accounts => {

    it("should set first account as owner", async () => {
        let instance = await CarContract.deployed();
        let owner = await instance.getOwner.call();
        
        //console.log(owner.valueOf());
        assert.equal(owner.valueOf(), accounts[0]);

    }); 


    it("create a new car", async() => {

        let instance = await CarContract.deployed();
        await instance.createCar(accounts[1], "WWWZZZ1JZ3W386752", 100, 1574181822, 1574281952, "LB-W6998", {from: accounts[0]});
        let results = await instance.showData.call(accounts[1], 0);

        //console.log(results);
        
        assert.equal(results[0], accounts[1], "wrong owner");
        assert.equal(results[1], "WWWZZZ1JZ3W386752", "wrong ");
        assert.equal(results[2], 100, "wrong mileage");
        assert.equal(results[3], 1574181822, "wrong tuev last date");
        assert.equal(results[4], 1574281952, "wrong maintenance date");
        assert.equal(results[5], "LB-W6998", "wrong license plate");

    });   

    it("update car data", async() => {

        let instance = await CarContract.deployed();
        await instance.createCar(accounts[1], "WWWZZZ1JZ3W386752", 100, 1574181822, 1574281952, "LB-W6998", {from: accounts[0]});
        await instance.updateCarData(accounts[1], 0, 200, 1574161922, 1574281822, {from: accounts[0]})
               
        let results = await instance.showData.call(accounts[1], 0);

        //console.log(results);
        
        assert.equal(results[0], accounts[1], "wrong owner");
        assert.equal(results[2], 200, "wrong mileage");
        assert.equal(results[3], 1574161922, "wrong tuev last date");
        assert.equal(results[4], 1574281822, "wrong maintenance date");
        assert.equal(results[5], "LB-W6998", "wrong license plate");


    });


    it("sell a car", async() => {

        let instance = await CarContract.deployed();
        await instance.createCar(accounts[1], "WWWZZZ1JZ3W386752",100, 1574181822, 1574281952, "LB-W6998", {from: accounts[0]});
        await instance.sellCar(accounts[2], 0, {from: accounts[1]});

        let results = await instance.showData.call(accounts[2], 0);

        //console.log(results);
        assert.equal(results[0], accounts[2], "wrong owner");
        assert.equal(results[5  ], "", "license plate not empty");

        let counter = await instance.getCounterCar.call(accounts[1]); 
        //console.log(counter);
        assert.equal(counter, 2, "not delete sold car");

        counter = await instance.getCounterCar.call(accounts[2]);
        //console.log(counter.valueOf());
        assert.equal(counter, 1, "car was not sold correctly");
        
    });


    it("set create rights", async() => {
        let instance = await CarContract.deployed();

        let returnValue = await instance.getRight.call(0, {from: accounts[1]});
        assert.equal(returnValue, false, "wrong ini from create right");
        
        await instance.setRight(true, false, accounts[1], {from: accounts[0]});

        returnValue = await instance.getRight.call(0, {from: accounts[1]});
        assert.equal(returnValue, true, "failed give change right to user");

    });

    it("set change right", async() => {
        let instance = await CarContract.deployed();

        let returnValue = await instance.getRight.call(1, {from: accounts[1]});
        assert.equal(returnValue, false, "wrong ini from change right");

        await instance.setRight(false , true, accounts[1], {from: accounts[0]});
        
        returnValue = await instance.getRight.call(1, {from: accounts[1]});
        assert.equal(returnValue, true, "failed give change right to user");

    });


}); 