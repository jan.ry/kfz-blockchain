pragma solidity ^0.5.8;

contract CarClass {

    address owner = msg.sender;

    struct Car  {
        address owner;
        string vehicleIN;
        uint mileage;
        uint32 tuevLastDate; // as Linux timestamp
        uint32 maintenanceDate; //as Linux timestamp
        string licensePlate;
    }

    mapping(address => Car[]) public car;

    mapping(address => bool) public createData;
    mapping(address => bool) public changeData;

    event CreateCar(address creater, address owner, uint index);
    event UpdateCar(address updater, address owner, uint index);
    event SellCar(address seller, address owner, uint index);
    event AddedLicensePlate(address creater, address owner, uint index);

    constructor() public {
        createData[owner] = true;
        changeData[owner] = true;
    }

    function setRight(bool create, bool change, address acc) public {

        require(msg.sender == owner, "only Owner can change a right");

        createData[acc] = create;
        changeData[acc] = change;

    }

    function getRight(int i) public view returns(bool){
        if(i == 0) {
            return createData[msg.sender];
        } else if(i == 1)
        {
            return changeData[msg.sender];
        }
    }

    function showData(address acc, uint256 i) public view returns (address, string memory, uint, uint32, uint32, string memory) {
        require(i < car[acc].length, "wrong index");
        return (car[acc][i].owner, car[acc][i].vehicleIN, car[acc][i].mileage, car[acc][i].tuevLastDate, car[acc][i].maintenanceDate, car[acc][i].licensePlate);
    }

    function createCar(address acc, string memory vIN, uint mileage, uint32 tuev, uint32 maintenance, string memory licensePlate) public {

        require(createData[msg.sender], "missing right to create a car");
        Car memory newCar;//= Car({owner: acc, mileage: mileage, tuevLastDate: tuev, maintenanceDate: maintenance, licensePlate: licensePlate});

        newCar.owner = acc;
        newCar.vehicleIN = vIN;
        newCar.mileage = mileage;
        newCar.tuevLastDate = tuev;
        newCar.maintenanceDate = maintenance;
        newCar.licensePlate = licensePlate;


        car[acc].push(newCar);

        emit CreateCar(msg.sender, acc, car[acc].length);
    }


    function addLicensePlate(address acc, uint256 i, string memory licensePlate) public {
        require(createData[msg.sender], "missing right to add licensePlate");
        require(i < car[acc].length, "wrong index");

        car[acc][i].licensePlate = licensePlate;

        emit AddedLicensePlate(msg.sender, acc, i);
    }

    function getCounterCar(address acc) public view returns (uint) {
        return car[acc].length;
    }


    function sellCar(address acc, uint256 i) public {
        require(i < car[msg.sender].length, "wrong index");
        car[acc].push(car[msg.sender][i]);
        car[acc][car[acc].length-1].owner = acc;
        car[acc][car[acc].length-1].licensePlate = "";

        car[msg.sender][i] = car[msg.sender][(car[msg.sender].length-1)];
        delete car[msg.sender][car[msg.sender].length-1];
        car[msg.sender].length--;

        emit SellCar(msg.sender, acc, car[msg.sender].length);
    }


    function updateCarData(address acc, uint256 i, uint mileage, uint32 tuev, uint32 maintenance) public {
        require(changeData[msg.sender], "missing right to change data");

        car[acc][i].mileage = mileage;
        if(tuev > 0) {
            car[acc][i].tuevLastDate = tuev;
        }

        if(maintenance > 0) {
            car[acc][i].maintenanceDate = maintenance;
        }

        emit UpdateCar(msg.sender, acc, i);
    }

    function getOwner() public view returns (address)
    {
        return owner;
    }

    function getStatus(address test) public view returns (int)
    {
        if(test == owner)
        {
            return 1;
        }
        else if (createData[test])
        {
            return 2;
        }
        else if (changeData[test])
        {
            return 3;
        }

        return 4;
    }


}
