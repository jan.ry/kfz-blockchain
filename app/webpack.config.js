const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: "./src/index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "dist"),
  },
  plugins: [
    new CopyWebpackPlugin([{ from: "./src/indexAdmin.html", to: "indexAdmin.html" }]),
    new CopyWebpackPlugin([{ from: "./src/index.html", to: "index.html" }]),
    new CopyWebpackPlugin([{ from: "./src/indexAuthority.html", to: "indexAuthority.html" }]),
    new CopyWebpackPlugin([{ from: "./src/indexUser.html", to: "indexUser.html" }]),
    new CopyWebpackPlugin([{ from: "./src/indexWorkshop.html", to: "indexWorkshop.html" }]),

  ],
  devServer: { contentBase: path.join(__dirname, "dist"), compress: true },
};
