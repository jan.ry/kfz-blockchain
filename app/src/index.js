import Web3 from "web3";
import carArtifact from "../../truffle/build/contracts/CarClass.json";

let showAllCars = false;
let onlyUser = false;
let newLoad = true;

const App = {
  web3: null,
  account: null,
  car: null,

  start: async function() {
    const { web3 } = this;

    try {
      // get contract instance
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = carArtifact.networks[networkId];
      this.car = new web3.eth.Contract(
        carArtifact.abi,
        deployedNetwork.address,
      );
      // get accounts
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];

      //need wait to check if you must load userData
      await this.getState(this.account);

      if(onlyUser) {
        let counter =  await this.getCounter(this.account);
        console.log("counter");
        console.log(counter)
        this.getCars(this.account, parseInt(counter));
      }

      console.log("start");
      console.log(accounts[0]); 

    } catch (error) {
      console.error("Could not connect to contract or chain.");
    }
  },

  getCounter: async function(address) {
    const { getCounterCar} = this.car.methods;
    const counterCar = await getCounterCar(address).call({ from: this.account });
  
    return await counterCar;
  },

  createCar: async function() {

    const { createCar } = this.car.methods;

    const licensePlate = document.getElementById("txtLicensePlate").value;
    const vin = document.getElementById("txtVin").value;
    const address = document.getElementById("txtUser").value;
    const mileage = document.getElementById("txtMileage").value;
    const lastTuev = document.getElementById("lastTuev").value;
    const lastMaintenance = document.getElementById("lastMaintenance").value;

    let dateParts = lastTuev.split('-');
    const dateTuev = new Date(parseInt(dateParts[0]), parseInt(dateParts[1])-1, parseInt(dateParts[2]));

    dateParts = lastMaintenance.split('-');
    const dateMaintenance = new Date(parseInt(dateParts[0]), parseInt(dateParts[1])-1, parseInt(dateParts[2]));

    await createCar(address, vin, mileage, dateTuev.getTime() / 1000, dateMaintenance.getTime() / 1000, licensePlate).send({ from: this.account });
    alert("Auto wurde erfolgreich erstellt!");
  
  },

  updateCar: async function() {

    const address = document.getElementById("txtUser").value;
    const carID = document.getElementById("txtCarID").value;
    const mileage = document.getElementById("txtMileage").value;

    const lastTuev = document.getElementById("lastTuev");
    const lastMaintenance = document.getElementById("lastMaintenance");


    let dateTuev = 0;
    let dateMaintenance = 0;

    if(lastTuev.value.length > 0)
    {
      console.log(lastTuev.value);
      let dateParts = lastTuev.value.split('-');
      dateTuev = (new Date(parseInt(dateParts[0]), parseInt(dateParts[1])-1, parseInt(dateParts[2])).getTime() / 1000);
    }

    if(lastMaintenance.value.length > 0)
    {
      console.log(lastMaintenance.value);
      let dateParts = lastMaintenance.value.split('-');
      dateMaintenance = (new Date(parseInt(dateParts[0]), parseInt(dateParts[1])-1, parseInt(dateParts[2])).getTime() / 1000);
    }


    const {updateCarData} = this.car.methods;
    await updateCarData(address, carID, mileage, dateTuev, dateMaintenance).send({from: this.account});

    alert("Daten wurden aktualisiert");

  },

  getCars: async function(address, counter){

    const {showData} = this.car.methods;

    let table = document.getElementById("table");
    let bodyTable = table.getElementsByTagName('tbody')[0];

    for (let i = 0; i < counter; ++i) {

      let row = bodyTable.insertRow(i);
      
      const results = await showData(address, i).call({from: this.account});

      console.log(results);

      let cell = row.insertCell(0);
      cell.innerHTML = i;

      for(let j = 0; j < 6; ++j) {
        let cell = row.insertCell(j+1);
        if((j == 3) || (j == 4)) {
          const date = new Date(parseInt(results[j])*1000);
          cell.innerHTML = date.getDate() + "." + (parseInt(date.getMonth()) + 1) + "." + date.getFullYear();
        }
        else {
          cell.innerHTML = results[j];
        }
      }
    }
  },

  addLicensePlate: async function() {

    const {addLicensePlate} = this.car.methods;
    
    const carNumber = document.getElementById("txtCar").value;
    const address = document.getElementById("txtUser2").value;
    const licensePlate = document.getElementById("txtLicensePlate2").value;

    console.log(licensePlate);

    await addLicensePlate(address, carNumber, licensePlate).send({from: this.account});

    alert("Kennzeichen wurde erfolgreich hinzugefügt.");

  },
  
  setRights: async function() {

    const address = document.getElementById("txtUserKey").value;

    const {setRight} = this.car.methods;
    await setRight(document.getElementById("ckInsertData").checked, document.getElementById("ckChangeData").checked, address).send({ from: this.account });

    alert("Rechte wurden erfolgreich gesetzt!");
  },

  getState: async function(address) {

    if(!showAllCars)
    {

      const {getStatus} = this.car.methods;
     
      const state = await getStatus(address).call({from: this.account});

      console.log("state: " + state);
      console.log(window.location.href);

     let loc = "";
  
      switch(parseInt(state))
      {
        case 1:
          loc = "/indexAdmin.html";
          break;
        case 2:
          loc = "/indexAuthority.html";
          break;
        case 3:
          loc = "/indexWorkshop.html";
          break;
        case 4:
          loc = "/indexUser.html";
          break;
  
      }

      if(window.location.href.search("indexShowAllCars") != -1)
      {
        showAllCars = true;
        return;
      } else if(window.location.href.search("indexUser.html") != -1)
      {
        onlyUser = true;
      }

      if(window.location.href.search(loc) == -1)
      {
        location.href = loc;
      }
    }
  },

  sellCar: async function() {

    const {sellCar} = this.car.methods;

    const carNumber = document.getElementById("txtCar").value;
    const address = document.getElementById("txtUser").value;

    await sellCar(address, carNumber).send({from: this.account});

    alert("Auto wurde erfolgreich verkauft!");

  },
};


//function is called when user in metamask changed 
window.ethereum.on('accountsChanged', function (accounts) {
  if(App.web3 != null)
  {
    App.getState(accounts[0]);
  }
 })

window.App = App;

window.addEventListener("load", function() {
  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable(); // get permission to access accounts
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
    );
  }

  App.start();
});
